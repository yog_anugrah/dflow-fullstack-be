const express = require('express')
const router = express.Router()
const User = require("../models/user.js")
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');

// email SMTP
const transporter = nodemailer.createTransport({
  port: 1025,
  host: "127.0.0.1",
});

const authenticateJWT = (req, res, next) => {
  const authHeader = req.headers.authorization;

  if (authHeader) {
    const token = authHeader.split(' ')[1];
    jwt.verify(token, process.env.TOKEN_SECRET, (err, user) => {
    if (err) {
      return res.sendStatus(403);
    }
      req.user = user;
      next();
    });
  } else {
    res.sendStatus(401);
  }
};

const authenticateJWTParam = (req, res, next) => {
  const authHeader = req.body.token;

  if (authHeader) {
    const token = authHeader;
    jwt.verify(token, process.env.TOKEN_SECRET, (err, user) => {
    if (err) {
      return res.sendStatus(403);
    }
      req.user = user;
      next();
    });
  } else {
    res.sendStatus(401);
  }
};

router.post("/forgot-password", function(request, response, next){
  User.findOne({email : request.body.email}).then(function(user){
    const userId = user._id
    const link = `http://localhost:3000/reset-password/`
    const mailData = {
      from : 'admin@city.run',
      to : `${user.email}`,
      text : `Seems Like You Request Forgot Password, Here is the link to reset password! <a href="${link}"> Here </a>`
    }
    transporter.sendMail(mailData, function(error, info){
      response.status(200).json({
        message : `SUCCESS`
      })
    })
  })
});

router.post("/reset-password", function(request, response, next) {
  User.findOneAndUpdate({email : request.body.email}, request.body, function (err, user){
    user.password = request.body.new_password;
    user.save().then(function(user){
      response.json({
        message : "password has been reset!"
      })
    })
  })
})

router.post("/register", function(request, response, next){
  const user = new User(request.body);
  try {
    user.save().then(function(user){
      const userId = user._id
      const link = `http://localhost:3000/active/${userId}`
      const mailData = {
        from : 'admin@city.run',
        to : `${user.email}`,
        text : `Welcome To City Run! Here is Your Activation link <a href="${link}"> Here </a>`
      }
      transporter.sendMail(mailData, function(error, info){
        response.status(200).json({
          message : `SUCCESS`
        })
      })
    })
  } catch (error) {
    response.status(500).json({
      message : `SERVER SIDE ERROR : ${error.message}`
    })
  }
});

router.post("/activate/:token", function(request, response, next){
  User.findOne({_id : request.params.token}).then(function(user){
    user.is_verify = true;
    user.save().then(function(user){
      response.json({
        message : "User Activated"
      })
    })
  })
})

router.post("/login", function(request, response, next){
  User.findOne({email : request.body.email}).then(function(user){
    // response.send(request.body.password)
    // console.log(user)
    if(!user.is_verify){
      response.status(500).json({
        message : "User Not Activated"
      })
    } else {
      
    }

    user.comparePassword(request.body.password, function(err, isMatch){
      console.log(isMatch)
      if(isMatch) {
        const accessToken = jwt.sign({email : user.email}, process.env.TOKEN_SECRET, { expiresIn: '30m' })
        User.findOneAndUpdate({email : request.body.email}, {is_login : true}, {new : true}, function(err, doc){
          const userData = user
          response.json({
            accessToken : accessToken,
            email : userData.email
          })
        })
      } else {
        response.status(500).json({
          message : 'user was wrong / not found'
        })
      }
    })
  })
})

router.post("/profile", authenticateJWTParam, function(request, response, next){
  User.findOne({email : request.body.email}).then(function(user) {
    response.json(user)
  })
})

router.post("/is-login", authenticateJWTParam, function(request, response, next){
  User.findOne({email : request.body.email}).then(function(user){
    // response.send(request.body.password)
    response.json({
      is_login : user.is_login
    })
  })
})

router.post("/logout", function(request, response, next){
  console.log(request.body.email, "sting")
  User.findOneAndUpdate({email : request.body.email}, {is_login : false}, {new : true}, function(){
    response.json({
      message : "Log Out Success!"
    })
  })
})


router.get("/userAll", function(request, response, next){
  User.find({}).then(function(user){

    response.json({
      list_user : user
    })
  })
})

router.post('/deleteAll', function(request, response, next){
  User.collection.deleteMany({})
  User.find({}).then(function(user){
    response.json({
      list_user : user
    })
  })
})

module.exports = router;