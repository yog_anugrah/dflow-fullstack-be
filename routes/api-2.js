const express = require('express')
const router = express.Router()
const Student = require('../models/student');

router.get('/students',function(req,res, next){
  Student.find({}).then(function(students){
    res.send(students)
  }).catch(next)
  // res.send({type: 'GET'});
});

router.get('/students/:name', function(req, res, next) {
  Student.findOne({_id : req.params.name}).then(function(student) {
    res.send(student)
    student.comparePassword('1', function(err, isMatch){
      if (err) throw err;
      console.log('1', isMatch);
    })
    // test a failing password
    student.comparePassword('123Password', function(err, isMatch) {
      if (err) throw err;
      console.log('123Password:', isMatch); // -&gt; 123Password: false
  });
  })
})

router.post('/students', function(req, res, next){
  const student = new Student(req.body);
  student.save(function (err){
    if (err) throw err
  })
  // Student.create(req.body).then(function(student) {
  //   res.send(student)
  // }).catch(next)
  
  // res.send({
  //     type: 'POST',
  //     name: req.body.name,
  //     roll: req.body.roll
  // });
});

router.put('/students/:id', function(req, res, next){
  Student.findOneAndUpdate({
    _id:req.params._id
  }, req.body).then(function (student){
    Student.findOne({_id : req.params.id}).then(function (student){
      res.send(student)
    })
  })
  // res.send({type: 'PUT'});
});
router.delete('/students/:id', function(req, res){
  Student.findOneAndDelete({_id : req.params.id}).then(function(student) {
    res.send(student)
  })
  // res.send({type: 'DELETE'});
});

module.exports = router;