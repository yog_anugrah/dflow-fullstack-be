const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const cors = require('cors');
// get config vars
dotenv.config();

// setup express
const app = express();

// ourdata is the name of the model we will be creating in MongoDb later.
// connect to mongo
mongoose.connect('mongodb://localhost/ourdata', {useNewUrlParser: true, useUnifiedTopology: true});
mongoose.Promise = global.Promise;

app.use(express.static('public'));

app.use(express.json());
app.use(express.urlencoded({ extended: true }))
app.use(cors());
// route list
app.use('/api/v1',require('./routes/api'));

// error handling middleware
app.use(function(err,req,res,next){
  res.status(422).send({error: err.message});
});

app.listen(process.env.port || 4000, function(){
  console.log('now listening for requests');
});

