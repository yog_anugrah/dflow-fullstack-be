const mongoose = require('mongoose');
const bcrypt = require('bcrypt')
const Schema = mongoose.Schema;
SALT_WORK_FACTOR = 10;

// create schema and model
const StudentSchema = new Schema({
  name : {
    type : "string"
  },
  roll : {
    type : 'string',
    required : [true, 'Roll Field Is Required, got {VALUE}']
  },
  present : {
    type : Boolean,
    default : true
  }
})

// try to hash roll data
StudentSchema.pre('save', function (next){
  let student = this
  
  // hash when user changed password / role
  if(!student.isModified('roll')) return next();

  //generate salt
  bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
    if (err) return next(err);

    // hash the password using new salt
    bcrypt.hash(student.roll, salt, function(err, hash) {
      if (err) return next(err);
      // override the cleartext password with the hashed one
      student.roll = hash;
      next();
  });
  })
})


// compare passwrod 
StudentSchema.methods.comparePassword = function(candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.roll, function(err, isMatch) {
      if (err) return cb(err);
      cb(null, isMatch);
  });
}

const Student = mongoose.model('Student', StudentSchema);

module.exports = Student