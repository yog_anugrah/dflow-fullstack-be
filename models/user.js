const mongoose = require('mongoose');
const bcrypt = require('bcrypt')
const Schema = mongoose.Schema;
SALT_WORK_FACTOR = 10;

// create schema
const UserSchema = new Schema({
  email : {
    type: String,
    required: true,
    match: /.+\@.+\..+/,
    unique: true
  },
  password : {
    type: String,
    required : true,
  },
  is_login : {
    type : Boolean,
    default : false
  },
  is_verify : {
    type : Boolean,
    default : false
  }
})

// before save, hash password first
UserSchema.pre('save', function(next) {
  const user = this;

  // only hash new password
  if (!user.isModified('password')) return next();

  // generate a salt
  bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
    if (err) return next(err);

    // hash the password using our new salt
    bcrypt.hash(user.password, salt, function(err, hash) {
        if (err) return next(err);
        // override the cleartext password with the hashed one
        user.password = hash;
        next();
    });
  });
})

UserSchema.methods.comparePassword = function(candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
      if (err) return cb(err);
      cb(null, isMatch);
  });
};

const User = mongoose.model('User', UserSchema);
module.exports = User